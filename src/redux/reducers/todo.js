import { ADD_TODO, DELETE_TODO, SELECT_TODO, DESELECT_TODO } from "../actions/actionTypes";

const initialState = {
    todos: [],
    movie: {
        title: "Jurassic Park"
    }
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TODO: 
            return {
                ...state,
                todos: state.todos.concat(action.todo)
            };
        default:
            return state;
    }
};

export default reducer;