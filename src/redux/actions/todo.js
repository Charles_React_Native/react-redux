import { ADD_TODO, DELETE_TODO, SELECT_TODO, DESELECT_TODO } from "./actionTypes";

export const addTodo = (todo) => {
    return {
        type: ADD_TODO,
        todo: todo
    }
}

export const deleteTodo = (id) => {
    return {
        type: DELETE_TODO,
        id: id
    }
}

export const selectTodo = (id) => {
    return {
        type: SELECT_TODO,
        id: id
    }
}

export const deselectTodo = (id) => {
    return {
        type: DESELECT_TODO,
        id: id
    }
}