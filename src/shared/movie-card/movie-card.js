import React from 'react';
import styled from "styled-components";
import styles from "./movie-card.style";
import { StyleSheet, Text, View } from 'react-native';

const StyledView = styled.View`
  ${styles}
`;

export default class MovieCard extends React.Component {
    render() {
        const { movie } = this.props;
        return (
            <StyledView>
                <Text>Movie Name: { movie.title }</Text>
            </StyledView>
        );
    }
}