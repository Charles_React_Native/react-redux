import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

import { connect } from "react-redux";
import { addTodo, selectTodo, deleteTodo, deselectTodo } from "./src/redux/actions/index";
import MovieCard from './src/shared/movie-card/movie-card';

class App extends React.Component {
    addTodo = todo => {
        this.props.onAddTodo({
            title: "Teste"
        });
    }

    listTodos() {
        return this.props.todos.map((t, i) => {
            return (
                <Text key={i.toString()}>{ t.title }</Text>
            );
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <Button
                    onPress={() => {
                        this.addTodo();
                    }}
                    title="Learn More"
                    color="#841584"
                    />
                { this.listTodos() }
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        todos: state.todo.todos
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAddTodo: (todo) => dispatch(addTodo(todo))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});