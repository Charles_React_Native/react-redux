import React from 'react';
import { Provider } from "react-redux";
import { StyleSheet, Text, View } from 'react-native';
import configStores from "./src/redux/configStores";
import configureStore from './src/redux/configStores';
import App from "./AppConnected";

const store = configureStore();
const ReduxComponent = () => (
  <Provider store={store}>
    <App />
  </Provider>
);

export default ReduxComponent;
